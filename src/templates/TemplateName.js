import React from 'react';
import PropTypes from 'prop-types';
import './TemplateName.module.scss';

const TemplateName = () => (
    <div className="TemplateName" data-testid="TemplateName">
        TemplateName Component
    </div>
);

TemplateName.propTypes = {};

TemplateName.defaultProps = {};

export default TemplateName;
