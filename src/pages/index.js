import Head from 'next/head'
import Card from "../components/Card/Card"

export default function Home() {
    return (
        <>
            <Head>
                <title>Klader.digital</title>
                <meta name="description" content="Blog of klader.digital"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <div className="container">
                <div className="row">
                    <div className="col-lg-3">
                        <Card/>
                    </div>
                    <div className="col-lg-6">
                        <Card/>
                    </div>
                    <div className="col-lg-3">
                        <Card/>
                    </div>
                </div>
            </div>
        </>
    )
}
