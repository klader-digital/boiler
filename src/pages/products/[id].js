import {useRouter} from "next/router";
import Image from 'next/image'

function Product({product}) {
    const router = useRouter()

    if (router.isFallback) {
        return <div>Loading...</div>
    }

    return (
        <div>
            <Image src={product.thumbnail} width={384} height={256}/>
            <h2>{product.id}: {product.title} {product.price}</h2>
            <p>{product.description}</p>
            <hr/>
        </div>
    )
}

export default Product

export async function getStaticProps(context) {
    const {params} = context
    const response = await fetch(`https://dummyjson.com/products/${params.id}`)
    const data = await response.json()

    return {
        props: {
            product: data
        },
        revalidate: 10, // In seconds
    }
}

export async function getStaticPaths() {
    return {
        paths: [{
            params: {id: '1'}
        }],
        fallback: true
    }
}