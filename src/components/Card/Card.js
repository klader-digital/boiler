import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styles from './Card.module.scss';

class Card extends Component {
    render() {

        console.log(this.props)

        return (
            <div className={styles.Card}>
                {this.props.title}
            </div>
        );
    }
}

Card.propTypes = {
    title: PropTypes.string,
    content: PropTypes.string
};

Card.defaultProps = {
    title: 'Lorem ipsum dolor sit amet.',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, itaque, quas! Alias dolorem iusto magnam minus nobis praesentium provident sint.'
};

export default Card;
