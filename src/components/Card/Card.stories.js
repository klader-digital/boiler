import Card from './Card';

export default {
    title: "Components/Card",
};

export const player = () => <Card/>;
player.story = {
    name: 'Player',
    parameters: {
        design: {
            type: 'figspec',
            url: 'https://www.figma.com/file/3Q1HTCalD0lJnNvcMoEw1x/Mealdrop?node-id=1906%3A3469',
        }
    }
};