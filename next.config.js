const path = require('path')

module.exports = {
    swcMinify: true,
    reactStrictMode: true,
    i18n: {
        locales: ["en"],
        defaultLocale: "en",
    },
    sassOptions: {
        includePaths: [path.join(__dirname, 'styles')],
    },
    images: {
        domains: [
            'dummyjson.com'
        ],
    },
}
