module.exports = {
    "stories": [
        "../src/components/**/*.stories.mdx",
        "../src/components/**/*.stories.@(js|jsx|ts|tsx)",
        "../src/pages/**/*.stories.mdx",
        "../src/pages/**/*.stories.@(js|jsx|ts|tsx)"
    ],
    "addons": [
        "@storybook/addon-links",
        "@storybook/addon-essentials",
        "@storybook/addon-interactions",
        "@storybook/addon-a11y",
        "@storybook/preset-scss",
        "storybook-addon-designs"
    ],
    "framework": "@storybook/react",
    "core": {
        "builder": "webpack5"
    }
}